package aggregate

import (
	"context"
	eventsV1 "order/internal/order/events/v1"
	"order/internal/order/models"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/log"
	"github.com/pkg/errors"
)

func (a *OrderAggregate) CreateOrder(ctx context.Context, shoptItems []*models.ShopItem, accountEmail, deliveryAddress string) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "OrderAggregate.CreateOrder")
	defer span.Finish()
	span.LogFields(log.String("AggregtateId", a.GetID()))

	if shoptItems == nil {
		return ErrOrderShopItemsIsRequired
	}

	if deliveryAddress == "" {
		return ErrInvalidDeliveryAddress
	}

	event, err := eventsV1.NewOrderCreatedEvent(a, shoptItems, accountEmail, deliveryAddress)
	if err != nil {
		return errors.Wrap(err, "NewOrderCreatedEvent")
	}

	// if err := event.SetMetadata(tracing.ExtractTextMapCarrier(span.Context())); err != nil {
	// 	tracing.TraceErr(span, err)
	// 	return errors.Wrap(err, "SetMetadata")
	// }

	return a.Apply(event)
}
