package models

import "github.com/google/uuid"

type ShopItem struct {
	id        uuid.UUID
	orderId   uuid.UUID
	productId uuid.UUID
	price     float64
	quantity  int
}

func (i ShopItem) Id() uuid.UUID {
	return i.id
}

func (i ShopItem) OrderId() uuid.UUID {
	return i.orderId
}

func (i ShopItem) ProductId() uuid.UUID {
	return i.productId
}

func (i ShopItem) Price() float64 {
	return i.price
}

func (i ShopItem) Quantity() int {
	return i.quantity
}

func NewItemWithId(
	id uuid.UUID,
	price float64,
	quantity int,
) (*ShopItem, error) {
	return &ShopItem{
		id:       id,
		price:    price,
		quantity: quantity,
	}, nil
}

func NewItem(
	price float64,
	quantity int,
) (*ShopItem, error) {
	return &ShopItem{
		id:       uuid.New(),
		price:    price,
		quantity: quantity,
	}, nil
}
