package v1

import (
	"order/internal/order/models"
	"order/pkg/es"
	"time"
)

const (
	OrderCreated   = "V1_ORDER_CREATED"
	OrderPaid      = "V1_ORDER_PAID"
	OrderSubmitted = "V1_ORDER_SUBMITTED"
	OrderCompleted = "V1_ORDER_COMPLETED"
	OrderCanceled  = "V1_ORDER_CANCELED"
)

type OrderCreatedEvent struct {
	ShopItems       []*models.ShopItem `json:"shopItems" bson:"shopItems,omitempty"`
	AccountEmail    string             `json:"accountEmail" bson:"accountEmail,omitempty"`
	DeliveryAddress string             `json:"deliveryAddress" bson:"deliveryAddress,omitempty"`
}

func NewOrderCreatedEvent(aggregate es.Aggregate, shopItems []*models.ShopItem, accountEmail, deliveryAddress string) (es.Event, error) {
	eventData := OrderCreatedEvent{
		ShopItems:       shopItems,
		AccountEmail:    accountEmail,
		DeliveryAddress: deliveryAddress,
	}

	event := es.NewBaseEvent(aggregate, OrderCreated)
	if err := event.SetJsonData(eventData); err != nil {
		return es.Event{}, nil
	}
	return event, nil
}

func NewOrderPaidEvent(aggregate es.Aggregate, payment *models.Payment) (es.Event, error) {
	event := es.NewBaseEvent(aggregate, OrderPaid)
	if err := event.SetJsonData(payment); err != nil {
		return es.Event{}, nil
	}
	return event, nil
}

func NewSubmitOrderEvent(aggregate es.Aggregate) (es.Event, error) {
	return es.NewBaseEvent(aggregate, OrderSubmitted), nil
}

type OrderCanceledEvent struct {
	CancelReason string `json:"cancelReason"`
}

func NewOrderCancelEvent(aggregate es.Aggregate, cancelReason string) (es.Event, error) {
	eventData := OrderCanceledEvent{CancelReason: cancelReason}
	event := es.NewBaseEvent(aggregate, OrderCanceled)
	if err := event.SetJsonData(eventData); err != nil {
		return es.Event{}, nil
	}
	return event, nil
}

type OrderCompletedEvent struct {
	DeliveryTimestamp time.Time `json:"deliveryTimestamp"`
}

func NewOrderCompletedEvent(aggregate es.Aggregate, deliveryTimestamp time.Time) (es.Event, error) {
	eventData := OrderCompletedEvent{DeliveryTimestamp: deliveryTimestamp}
	event := es.NewBaseEvent(aggregate, OrderCompleted)
	if err := event.SetJsonData(eventData); err != nil {
		return es.Event{}, nil
	}
	return event, nil
}
