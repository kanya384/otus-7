package commands

import (
	"order/internal/order/models"
	"order/pkg/es"
	"time"
)

type CreateOrderCommand struct {
	es.BaseCommand
	ShopItems       []*models.ShopItem `json:"shopItems" bson:"shopItems,omitempty" validate:"required"`
	AccountEmail    string             `json:"accountEmail" bson:"accountEmail,omitempty" validate:"required,email"`
	DeliveryAddress string             `json:"deliveryAddress" bson:"deliveryAddress,omitempty" validate:"required"`
}

func NewCreateOrderCommand(aggregateId string, shopItems []*models.ShopItem, acountEmail, deliveryAddress string) *CreateOrderCommand {
	return &CreateOrderCommand{BaseCommand: es.NewBaseCommand(aggregateId), ShopItems: shopItems, AccountEmail: acountEmail, DeliveryAddress: deliveryAddress}
}

type PayOrderCommand struct {
	models.Payment
	es.BaseCommand
}

func NewPayOrderCommand(payment models.Payment, aggregateId string) *PayOrderCommand {
	return &PayOrderCommand{Payment: payment, BaseCommand: es.NewBaseCommand(aggregateId)}
}

type SubmitOrderCommand struct {
	es.BaseCommand
	ShopItems []*models.ShopItem `json:"shopItems" bson:"shopItems,omitempty" validate:"required"`
}

func NewSubmitOrderCommand(aggregateId string) *SubmitOrderCommand {
	return &SubmitOrderCommand{BaseCommand: es.NewBaseCommand(aggregateId)}
}

type CancleOrderCommand struct {
	es.BaseCommand
	CancelReason string `json:"cancelReason" validate:"required"`
}

func NewCancelOrderCommand(aggregateId string, cancelReason string) *CancleOrderCommand {
	return &CancleOrderCommand{BaseCommand: es.NewBaseCommand(aggregateId), CancelReason: cancelReason}
}

type CompleteOrderCommand struct {
	es.BaseCommand
	DeliveryTimestamp time.Time `json:"deliveryTimestamp" validate:"required"`
}

func NewCompleteDeliveryCommand(aggregateId string, deliveryTimestamp time.Time) *CompleteOrderCommand {
	return &CompleteOrderCommand{BaseCommand: es.NewBaseCommand(aggregateId), DeliveryTimestamp: deliveryTimestamp}
}
