package commands

import (
	"context"
	"errors"
	"order/pkg/es"

	"order/internal/order/aggregate"

	"github.com/AleksK1NG/es-microservice/pkg/logger"
	"github.com/EventStore/EventStore-Client-Go/esdb"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/log"
)

type CreateOrderCommandHandler interface {
	Handle(context.Context, *CreateOrderCommand) error
}

type createOrderHandler struct {
	log logger.Logger
	es  es.AggregateStore
}

func NewCreateOrderHandler(log logger.Logger, es es.AggregateStore) *createOrderHandler {
	return &createOrderHandler{log: log, es: es}
}

func (c *createOrderHandler) Handle(ctx context.Context, command *CreateOrderCommand) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "createOrderHandler.Handle")
	defer span.Finish()
	span.LogFields(log.String("AggregateID", command.GetAggregateID()))

	order := aggregate.NewOrderAggregateWithId(command.AggregateID)
	err := c.es.Exists(ctx, order.GetID())
	if err != nil && !errors.Is(err, esdb.ErrStreamNotFound) {
		return err
	}

	if err := order.CreateOrder(ctx, command.ShopItems, command.AccountEmail, command.DeliveryAddress); err != nil {
		return err
	}

	span.LogFields(log.String("order", order.String()))
	return c.es.Save(ctx, order)
}
