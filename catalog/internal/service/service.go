package service

import (
	"catalog/internal/service/adapters/repository"
)

type service struct {
	repository repository.Repository
}

func New(repository repository.Repository) (*service, error) {
	return &service{
		repository: repository,
	}, nil
}
