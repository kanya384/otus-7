package repository

import (
	"catalog/internal/domain/category"
	"catalog/internal/domain/product"
	"context"

	"github.com/google/uuid"
)

type Repository interface {
	CategoryRepository
	ProductRepository
}

type CategoryRepository interface {
	CreateCategory(ctx context.Context, category *category.Category) (err error)
	DeleteCategory(ctx context.Context, id uuid.UUID) (err error)
	ReadCategoryById(ctx context.Context, id uuid.UUID) (category *category.Category, err error)
	ReadCatgoriesList(ctx context.Context) (categories []*category.Category, err error)
}

type ProductRepository interface {
	CreateProduct(ctx context.Context, product *product.Product) (err error)
	DeleteProduct(ctx context.Context, id uuid.UUID) (err error)
	ReadProductById(ctx context.Context, id uuid.UUID) (product *product.Product, err error)
	ReadProductsOfCategory(ctx context.Context, categoryId uuid.UUID) (product []*product.Product, err error)
}
