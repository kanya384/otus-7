package product

import (
	"time"

	"github.com/google/uuid"
)

type Product struct {
	id         uuid.UUID
	categoryId uuid.UUID
	name       string
	price      float64
	createdAt  time.Time
	modifiedAt time.Time
}

func (p Product) Id() uuid.UUID {
	return p.id
}

func (p Product) CategoryId() uuid.UUID {
	return p.categoryId
}

func (p Product) Name() string {
	return p.name
}

func (p Product) Price() float64 {
	return p.price
}

func (p Product) CreatedAt() time.Time {
	return p.createdAt
}

func (p Product) ModifiedAt() time.Time {
	return p.modifiedAt
}

func NewProductWithId(
	id uuid.UUID,
	name string,
	price float64,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Product, error) {
	return &Product{
		id:         id,
		name:       name,
		price:      price,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func NewProduct(
	name string,
	price float64,
) (*Product, error) {
	return &Product{
		id:         uuid.New(),
		name:       name,
		price:      price,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}
