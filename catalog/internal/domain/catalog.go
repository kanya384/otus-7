package domain

import "catalog/internal/domain/category"

type Catalog struct {
	categories []*category.Category
}

func NewCatalog(categories []*category.Category) *Catalog {
	return &Catalog{
		categories: categories,
	}
}

func (c Catalog) Categories() []*category.Category {
	return c.categories
}

func (c *Catalog) CreateCategoryInCatalog(category *category.Category) {
	c.categories = append(c.categories, category)
}
