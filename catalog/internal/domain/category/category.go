package category

import (
	"catalog/internal/domain/product"
	"time"

	"github.com/google/uuid"
)

type Category struct {
	id         uuid.UUID
	name       string
	products   []*product.Product
	createdAt  time.Time
	modifiedAt time.Time
}

func (c *Category) Id() uuid.UUID {
	return c.id
}

func (c *Category) Name() string {
	return c.name
}

func (c *Category) Products() []*product.Product {
	return c.products
}

func (c *Category) CreatedAt() time.Time {
	return c.createdAt
}

func (c *Category) ModifiedAt() time.Time {
	return c.modifiedAt
}

func NewCategoryWithId(
	id uuid.UUID,
	name string,
	products []*product.Product,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Category, error) {
	return &Category{
		id:         id,
		name:       name,
		products:   products,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func NewCategory(
	name string,
	products []*product.Product,
) (*Category, error) {
	return &Category{
		id:         uuid.New(),
		name:       name,
		products:   products,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}
