package repository

import (
	"catalog/internal/domain/product"
	"catalog/internal/repository/dao"
	"catalog/pkg/transaction"
	"context"
	"errors"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	productsTable = "product"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"product_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
)

func (r *Repository) CreateProduct(ctx context.Context, product *product.Product) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	rawQuery := r.Builder.Insert(productsTable).Columns(dao.ProductColumns...).Values(product.Id(), product.CategoryId(), product.Name(), product.CreatedAt(), product.ModifiedAt())
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return
}

func (r *Repository) DeleteProduct(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	rawQuery := r.Builder.Delete(productsTable).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) ReadProductById(ctx context.Context, id uuid.UUID) (product *product.Product, err error) {
	panic("not implemented") // TODO: Implement
}

func (r *Repository) ReadProductsOfCategory(ctx context.Context, categoryId uuid.UUID) (product []*product.Product, err error) {
	panic("not implemented") // TODO: Implement
}
