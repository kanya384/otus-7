package dao

import (
	"time"

	"github.com/google/uuid"
)

type Product struct {
	Id         uuid.UUID `db:"id"`
	CategoryId uuid.UUID `db:"category_id"`
	Name       string    `db:"name"`
	Price      float64   `db:"price"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ProductColumns = []string{
	"id",
	"category_id",
	"name",
	"price",
	"created_at",
	"modified_at",
}
