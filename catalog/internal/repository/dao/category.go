package dao

import (
	"time"

	"github.com/google/uuid"
)

type Category struct {
	Id         uuid.UUID `db:"id"`
	Name       string    `db:"name"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var CategoryColumns = []string{
	"id",
	"name",
	"created_at",
	"modifed_at",
}
