package repository

import (
	"gitlab.com/kanya384/gotools/psql"
)

type Repository struct {
	*psql.Postgres
}

type Options struct {
}

func New(pg *psql.Postgres) (*Repository, error) {
	var r = &Repository{pg}
	return r, nil
}
