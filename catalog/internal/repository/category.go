package repository

import (
	"catalog/internal/domain/category"
	"context"

	"github.com/google/uuid"
)

const (
	categoryTableName = "category"
)

func (r *Repository) CreateCategory(ctx context.Context, category *category.Category) (err error) {
	panic("not implemented") // TODO: Implement
}

func (r *Repository) DeleteCategory(ctx context.Context, id uuid.UUID) (err error) {
	panic("not implemented") // TODO: Implement
}

func (r *Repository) ReadCategoryById(ctx context.Context, id uuid.UUID) (category *category.Category, err error) {
	panic("not implemented") // TODO: Implement
}

func (r *Repository) ReadCatgoriesList(ctx context.Context) (categories []*category.Category, err error) {
	panic("not implemented") // TODO: Implement
}
